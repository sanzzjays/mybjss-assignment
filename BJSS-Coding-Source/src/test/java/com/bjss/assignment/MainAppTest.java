package com.bjss.assignment;

import static org.junit.Assert.assertTrue;

import com.bjss.assignment.Rules.OfferStorage;
import com.bjss.assignment.product.*;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Unit test for simple App.
 */
public class MainAppTest
{

    @Test
    public void loadProducts()
    {

        ProductStorage productStorage = new ProductStorage();
        final HashMap<String, ProductItem> productDetailHash = productStorage.getProductDetailHash();
        assertTrue( productDetailHash.containsKey("BREAD") );
        assertTrue( productDetailHash.containsKey("MILK") );
        assertTrue( productDetailHash.containsKey("SOUP") );
        assertTrue( productDetailHash.containsKey("APPLES") );
    }

    @Test
    public void loadPriceBasket(){

        PriceBasket priceBasket = new PriceBasket();
        ProductItem productItem1 = new ProductItem("BREAD", 0.80);
        SaleItem saleItem1 = new SaleItem(productItem1);

        ProductItem productItem2 = new ProductItem("MILK", 1.30);
        SaleItem saleItem2 = new SaleItem(productItem2);

        ProductItem productItem3 = new ProductItem("SOUP", 0.65);
        SaleItem saleItem3 = new SaleItem(productItem3);

        priceBasket.addListProductSaleItems(saleItem1);
        priceBasket.addListProductSaleItems(saleItem2);
        priceBasket.addListProductSaleItems(saleItem3);

        List<SaleItem> saleItemList = priceBasket.getListProductSaleItems();
        assertTrue( saleItemList.contains(saleItem1) );
    }

    @Test
    public void testDiscountPriceOnApples(){
        ProductStorage productStorage = new ProductStorage();
        final HashMap<String, ProductItem> productDetailHash = productStorage.getProductDetailHash();

        PriceBasket priceBasket = new PriceBasket();
        ProductItem productItem1 = new ProductItem("APPLES", 1.00);
        SaleItem saleItem1 = new SaleItem(productItem1);
        priceBasket.addListProductSaleItems(saleItem1);
        OfferStorage OfferStorage = new OfferStorage(productStorage, priceBasket);
        Locale.setDefault(new Locale("en", "UK"));
        Calculator calculator = new Calculator();
        double subTotal = calculator.calculateSubTotal(priceBasket, OfferStorage, productDetailHash);
        assertTrue( subTotal == 0.90 );
    }

    @Test
    public void testOfferOnSoup(){
        ProductStorage productStorage = new ProductStorage();
        final HashMap<String, ProductItem> productDetailHash = productStorage.getProductDetailHash();

        PriceBasket priceBasket = new PriceBasket();
        ProductItem productItem1 = new ProductItem("SOUP", 0.65);
        SaleItem saleItem1 = new SaleItem(productItem1);
        priceBasket.addListProductSaleItems(saleItem1);

        ProductItem productItem2 = new ProductItem("SOUP", 0.65);
        SaleItem saleItem2 = new SaleItem(productItem2);
        priceBasket.addListProductSaleItems(saleItem2);

        OfferStorage OfferStorage = new OfferStorage(productStorage, priceBasket);
        Locale.setDefault(new Locale("en", "UK"));
        Calculator calculator = new Calculator();
        double subTotal = calculator.calculateSubTotal(priceBasket, OfferStorage, productDetailHash);
        assertTrue( subTotal == 1.7000000059604645 );
    }

    @Test
    public void testATypicalSale(){

        ProductStorage productStorage = new ProductStorage();
        final HashMap<String, ProductItem> productDetailHash = productStorage.getProductDetailHash();

        PriceBasket priceBasket = new PriceBasket();

        ProductItem productItem1 = new ProductItem("SOUP", 0.65);
        SaleItem saleItem1 = new SaleItem(productItem1);
        priceBasket.addListProductSaleItems(saleItem1);

        ProductItem productItem2 = new ProductItem("MILK", 1.30);
        SaleItem saleItem2 = new SaleItem(productItem2);
        priceBasket.addListProductSaleItems(saleItem2);

        ProductItem productItem3 = new ProductItem("BREAD", 0.80);
        SaleItem saleItem3 = new SaleItem(productItem3);
        priceBasket.addListProductSaleItems(saleItem2);

        ProductItem productItem4 = new ProductItem("BREAD", 0.80);
        SaleItem saleItem4= new SaleItem(productItem4);
        priceBasket.addListProductSaleItems(saleItem4);

        OfferStorage OfferStorage = new OfferStorage(productStorage, priceBasket);
        Locale.setDefault(new Locale("en", "UK"));
        Calculator calculator = new Calculator();
        double subTotal = calculator.calculateSubTotal(priceBasket, OfferStorage, productDetailHash);
        assertTrue( subTotal == 4.05);
    }
}
