package com.bjss.assignment.main;

import com.bjss.assignment.Rules.OfferStorage;
import com.bjss.assignment.product.*;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Main Class
 *
 */
public class MainApp
{

    private static Logger logger = Logger.getLogger(MainApp.class.getName());
    private static final String CURRENCY = "£";

    public static void main( String[] args )
    {


        if(args.length == 0 || args.length == 1) {
            System.out.print("Please enter the inputs: Example:  PriceBasket item1, item2");
            return;
        }

        if(args.length >= 2 ){

            //Load the Products from properties file
            ProductStorage productStorage = new ProductStorage();
            HashMap<String, ProductItem> hashOfProducts = productStorage.getProductDetailHash();

            //Create a Price Basket for shopping
            PriceBasket priceBasket = new PriceBasket();

            for (String arg: args){
                if(!arg.equalsIgnoreCase("PriceBasket") && hashOfProducts.containsKey(arg.toUpperCase())){

                    ProductItem productItem = hashOfProducts.get(arg.toUpperCase());
                    SaleItem saleItem = new SaleItem(productItem);
                    //add the Product in the Shopping Basket based on the input
                    priceBasket.addListProductSaleItems(saleItem);

                }
            }
            //Check the Offers on the Product items purchased
            OfferStorage OfferStorage = new OfferStorage(productStorage, priceBasket);
            //Set Locale to be UK
            Locale.setDefault(new Locale("en", "UK"));
            //Get ready to Calculate the Shopping Cart or Basket, with Offers if any
            Calculator calculator = new Calculator();
            double subTotal = calculator.calculateSubTotal(priceBasket, OfferStorage, hashOfProducts);
            logger.info(String.format("Subtotal: %s %.2f", CURRENCY, subTotal));

        }


    }
}
