package com.bjss.assignment.product;

import java.util.ArrayList;
import java.util.List;

public class PriceBasket {

    private List<SaleItem> listProductSaleItems = new ArrayList<>();

    public List<SaleItem> getListProductSaleItems() {
        return listProductSaleItems;
    }

    public void addListProductSaleItems(SaleItem saleItem) {
        this.getListProductSaleItems().add(saleItem);
    }
}
