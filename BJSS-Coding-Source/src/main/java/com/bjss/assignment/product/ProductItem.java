package com.bjss.assignment.product;

public class ProductItem {

    private String name;
    private double pricePerUnit;
    private Unit unit;

    public ProductItem(){

    }
    public ProductItem(String name, double pricePerUnit){
        this.name = name;
        this.pricePerUnit = pricePerUnit;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
