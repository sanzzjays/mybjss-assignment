package com.bjss.assignment.product;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

/**
 * This Class is responsible for loading all the Products from the properties file
 */
public class ProductStorage {

    Logger logger = Logger.getLogger(ProductStorage.class.getName());

    private HashMap<String, ProductItem> productDetailHash = new HashMap<>();

    public ProductStorage(){

        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("src/main/resources/product_price_details.properties"));
            Set<Object> keys =  prop.keySet();
            for (Object key : keys) {
                String stringKey = (String)key;
                String value = prop.getProperty(stringKey);
                String valueSplit[] = value.split(",");
                String price = valueSplit[0];
                String unit = valueSplit[1];

                ClassLoader classLoader = this.getClass().getClassLoader();
                try {
                    Class clazz = classLoader.loadClass("com.bjss.assignment.product."+stringKey);
                    ProductItem productItem = (ProductItem)clazz.newInstance();
                    productItem.setName(stringKey.toUpperCase());
                    productItem.setPricePerUnit(Float.valueOf(price));
                    productItem.setUnit(Unit.valueOf(unit));

                    productDetailHash.put(stringKey.toUpperCase(), productItem);

                } catch (ClassNotFoundException e) {
                    logger.warning("Could not find the Class " + e);
                } catch (IllegalAccessException e) {
                    logger.warning("IllegalAccessException" + e);
                } catch (InstantiationException e) {
                    logger.warning("InstantiationException" + e);
                }
            }

        } catch (IOException e) {
            logger.warning("Could not find the properties file " + e);
        }

    }

    public HashMap<String, ProductItem> getProductDetailHash() {
        return productDetailHash;
    }
}
