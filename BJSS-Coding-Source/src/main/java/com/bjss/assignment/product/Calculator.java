package com.bjss.assignment.product;

import com.bjss.assignment.Rules.OfferAnotherProductForLessPrice;
import com.bjss.assignment.Rules.OfferStorage;
import com.bjss.assignment.Rules.ProductDiscountOffer;
import com.bjss.assignment.main.MainApp;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Calculator {

    private static Logger logger = Logger.getLogger(Calculator.class.getName());

    public Calculator() {

    }

    public double calculateSubTotal(PriceBasket priceBasket, OfferStorage offerStorage, HashMap<String, ProductItem>
            hashOfProducts) {

        double subTotal = 0;
        final List<SaleItem> listProductSaleItems = priceBasket.getListProductSaleItems();
        boolean flagSoup = false;
        for (SaleItem saleItem : listProductSaleItems) {

            if (offerStorage.getOffersHash().containsKey("APPLES") &&
                    priceBasket.getListProductSaleItems().stream().filter(
                            si ->si.getProductItem().getName().equalsIgnoreCase("APPLES")).collect(Collectors.toList()).size() > 0 ) {

                ProductDiscountOffer productDiscountOffer = (ProductDiscountOffer) offerStorage.getOffersHash().get(saleItem.getProductItem().getName());
                if (saleItem!=null && saleItem.getProductItem() != null && productDiscountOffer!=null) {
                    double discountedPrice = productDiscountOffer.priceAfterOffer(saleItem.getProductItem().getPricePerUnit());
                    logger.info(saleItem.getProductItem().getName() + productDiscountOffer.getDiscountPercentage() + " % off :" + (saleItem.getProductItem().getPricePerUnit() - saleItem.getProductItem().getPricePerUnit() - discountedPrice) * 100 + "p");
                    subTotal = subTotal + saleItem.getProductItem().getPricePerUnit() - discountedPrice;
                    continue;
                }

            }
            if (flagSoup!=true && offerStorage.getOffersHash().containsKey("SOUP") &&
                    priceBasket.getListProductSaleItems().stream().
                            filter(
                            si ->si.getProductItem().getName().
                              equalsIgnoreCase("SOUP")).collect(Collectors.toList()).size() == 2  ) {

                OfferAnotherProductForLessPrice offerAnotherProductForLessPrice = (OfferAnotherProductForLessPrice) offerStorage.getOffersHash().get(saleItem.getProductItem().getName());
                ProductItem newSaleItem = offerAnotherProductForLessPrice.priceAfterOffer(
                        saleItem.getProductItem(), priceBasket, hashOfProducts);
                ProductItem originalBread = hashOfProducts.get("BREAD");
                //listProductSaleItems.add(saleItem1);
                subTotal = subTotal + newSaleItem.getPricePerUnit() +
                        saleItem.getProductItem().getPricePerUnit() * 2;

                double savingsinPence =  (originalBread.getPricePerUnit() - newSaleItem.getPricePerUnit() ) * 100;
                logger.info(saleItem.getProductItem().getName() +  " offer - for 2 Soup tins, a Bread loaf for half price - you save :" +String.format("%.2f", savingsinPence) + "p");

                flagSoup = true;
                continue;
            }

            if(flagSoup == true){
                return subTotal;
            }
            subTotal = subTotal + saleItem.getProductItem().getPricePerUnit();
        }
        return subTotal;
    }
}
