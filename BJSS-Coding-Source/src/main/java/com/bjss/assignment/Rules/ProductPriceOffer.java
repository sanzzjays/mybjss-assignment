package com.bjss.assignment.Rules;

import com.bjss.assignment.product.PriceBasket;
import com.bjss.assignment.product.ProductItem;
import com.bjss.assignment.product.SaleItem;

import java.util.HashMap;

public interface ProductPriceOffer {
    public ProductItem priceAfterOffer(ProductItem productItem1, PriceBasket priceBasket,
                                    HashMap<String, ProductItem>
                                         hashOfProducts);
}
