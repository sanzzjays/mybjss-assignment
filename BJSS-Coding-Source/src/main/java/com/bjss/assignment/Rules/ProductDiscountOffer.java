package com.bjss.assignment.Rules;

public class ProductDiscountOffer implements Offer {

    private String nameOfProduct = null;
    private int discountPercentage = 0;

    public ProductDiscountOffer(String nameOfProduct, int discountPercentage) {

        this.nameOfProduct = nameOfProduct;
        this.discountPercentage = discountPercentage;
    }

    @Override
    public double priceAfterOffer(double productPrice) {

        return productPrice * discountPercentage / 100;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }
}
