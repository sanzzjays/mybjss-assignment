package com.bjss.assignment.Rules;

import com.bjss.assignment.product.ProductItem;

public interface Offer {

    public double priceAfterOffer(double productPrice);


}
