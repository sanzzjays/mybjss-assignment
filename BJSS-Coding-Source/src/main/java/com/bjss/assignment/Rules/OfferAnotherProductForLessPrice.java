package com.bjss.assignment.Rules;

import com.bjss.assignment.product.Bread;
import com.bjss.assignment.product.PriceBasket;
import com.bjss.assignment.product.ProductItem;
import com.bjss.assignment.product.SaleItem;

import java.util.HashMap;
import java.util.stream.Collectors;

public class OfferAnotherProductForLessPrice implements ProductPriceOffer{

    private String nameOfProduct = null;

    public OfferAnotherProductForLessPrice(String nameOfProduct){
        this.nameOfProduct = nameOfProduct;
    }

    @Override
    public ProductItem priceAfterOffer(ProductItem productItem1, PriceBasket priceBasket,
                                 HashMap<String, ProductItem>
                                         hashOfProducts) {
        SaleItem saleItem = null;
        ProductItem newBread =null;
        if(productItem1.getName().equalsIgnoreCase("SOUP")){

            if(priceBasket.getListProductSaleItems().stream().filter
                    (si-> si.getProductItem().getName().equalsIgnoreCase("SOUP")).
                    collect(Collectors.toList()).size() ==2) {

                   ProductItem originalBread = hashOfProducts.get("BREAD");

                   newBread = new ProductItem("BREAD", 0.5 * originalBread.getPricePerUnit());
                   //ProductItem bread = new Bread("BREAD", 1/2 * originalBread.getPricePerUnit());
                   saleItem = new SaleItem(newBread);
                   //priceBasket.addListProductSaleItems(saleItem);
                  return newBread;
            }
        }
        return newBread;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }
}
