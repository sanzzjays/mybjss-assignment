package com.bjss.assignment.Rules;

import com.bjss.assignment.product.PriceBasket;
import com.bjss.assignment.product.ProductItem;
import com.bjss.assignment.product.ProductStorage;

import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

public class OfferStorage {

    private HashMap<String, Object> offersHash = new HashMap<>();

    public OfferStorage() {

    }

    public OfferStorage(ProductStorage productStorage, PriceBasket priceBasket) {

        HashMap<String, ProductItem> productStorageHash = productStorage.getProductDetailHash();
        Set<String> productKeys = productStorageHash.keySet();

        ProductDiscountOffer productOffer = new ProductDiscountOffer("APPLES", 10);
        OfferAnotherProductForLessPrice offerAnotherProductForLessPrice = new
                OfferAnotherProductForLessPrice("SOUP");

        if (productKeys.contains(productOffer.getNameOfProduct()) &&
                priceBasket.getListProductSaleItems().stream().filter
                        (si -> si.getProductItem().getName().equalsIgnoreCase(productOffer.getNameOfProduct())).
                        collect(Collectors.toList()).size() > 0) {
            offersHash.put(productOffer.getNameOfProduct().toUpperCase(), productOffer);
        }

        if (productKeys.contains(offerAnotherProductForLessPrice.getNameOfProduct())
                &&
                priceBasket.getListProductSaleItems().stream().filter
                        (si -> si.getProductItem().getName().equalsIgnoreCase(offerAnotherProductForLessPrice.getNameOfProduct())).
                        collect(Collectors.toList()).size() > 0) {
            offersHash.put(offerAnotherProductForLessPrice.getNameOfProduct().toUpperCase(), offerAnotherProductForLessPrice);
        }
    }

    public HashMap<String, Object> getOffersHash() {
        return offersHash;
    }
}
